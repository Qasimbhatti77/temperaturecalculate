<?php

$temperature = array(32.3, 31.3, 28.2, 29.3, 29.7, 29.9, 28.7, 28.4, 30.5, 30.5, 31.7, 30.6, 29.4, 32.0, 36.2, 31.3, 32.8, 33.3, 32.9, 28.8, 30.8, 28.0, 25.9, 30.8, 32.4, 32.0, 31.3, 25.2, 29.1, 28.6, 30.6);
// Calculate the average temperature
$avg= array_sum($temperature) / count($temperature);
echo "January Average temperature " . round($avg, 2) . "\n";
// Sort the temperatures in ascending order

sort($temperature);
// Get the 5 lowest temperatures
$lowest = array_slice($temperature, 0, 5);
echo "January 5 Lowest temperatures :" . implode(", ", $lowest). "\n";

// Get the 5 highest temperatures
$highest = array_slice($temperature, -5);
echo "January 5 highest tempratures :" . implode(", ", $highest);
?>


